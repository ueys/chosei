package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Event struct {
	ID          bson.ObjectId `bson:"_id"        json:"_id"`
	Title       string        `bson:"title"      json:"title"`
	Description string        `bson:"desc"       json:"desc"`
	Candidates  []string      `bson:"candidates" json:"candidates"`
	Members     []Member      `bson:"members"    json:"members"`
}

type Member struct {
	Name   string   `bson:"name"   json:"name"`
	Status []string `bson:"status" json:"status"`
}

func getGinHost() string {
	// example $ export CHOSEI_GIN_HOST="localhost:80"
	host := os.Getenv("CHOSEI_GIN_HOST")
	if host == "" {
		host = "localhost:8080"
	}
	return host
}

func getMongoHost() string {
	// example $ export CHOSEI_MONGO_HOST="localhost:27017"
	host := os.Getenv("CHOSEI_MONGO_HOST")
	if host == "" {
		host = "localhost:27017"
	}
	return host
}

func runWebServer(db *mgo.Database) {
	host := getGinHost()
	router := gin.Default()
	router.Delims("{[{", "}]}")
	router.LoadHTMLGlob("templates/*.html")
	router.Static("events/css", "css")
	router.Static("events/js", "js")

	// view routers
	// it can use template, but not used
	view := router.Group("/events")
	{
		view.GET("/", func(c *gin.Context) {
			c.HTML(http.StatusOK, "index.html", gin.H{})
		})
		view.GET("/create", func(c *gin.Context) {
			c.HTML(http.StatusOK, "create.html", gin.H{})
		})
		view.GET("/view/:id", func(c *gin.Context) {
			c.HTML(http.StatusOK, "view.html", gin.H{})
		})
	}

	// api routers
	// TODO: fix http responce code
	v1 := router.Group("/api/v1")
	{
		// read all event
		v1.GET("/events", func(c *gin.Context) {
			var events []Event
			query := db.C("events").Find(bson.M{})
			query.All(&events)
			c.JSON(http.StatusOK, events)
		})
		// create new event
		v1.POST("/events", func(c *gin.Context) {
			var newEvent Event
			// TODO json validate
			if err := c.ShouldBindJSON(&newEvent); err == nil {
				newEvent.ID = bson.NewObjectId()
				db.C("events").Insert(newEvent)
				c.JSON(http.StatusOK, newEvent)
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		})

		// read event from id
		v1.GET("/events/:id", func(c *gin.Context) {
			// check id
			qid := c.Param("id")
			if !bson.IsObjectIdHex(qid) {
				c.JSON(http.StatusBadRequest, gin.H{"error": "not event id"})
			}
			id := bson.ObjectIdHex(qid)
			// find event from objcet id
			event := new(Event)
			if err := db.C("events").FindId(id).One(event); err == nil {
				c.JSON(http.StatusOK, event)
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		})
		// update event
		v1.PUT("/events/:id", func(c *gin.Context) {
			// check id
			qid := c.Param("id")
			if !bson.IsObjectIdHex(qid) {
				c.JSON(http.StatusBadRequest, gin.H{"error": "not event id"})
			}
			id := bson.ObjectIdHex(qid)
			// update object
			var updateEvent Event
			if err := c.ShouldBindJSON(&updateEvent); err == nil {
				updateEvent.ID = id
				if err := db.C("events").UpdateId(id, updateEvent); err == nil {
					c.JSON(http.StatusOK, updateEvent)
				} else {
					c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				}
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		})
		// delete event
		v1.DELETE("/events/:id", func(c *gin.Context) {
			// check id
			qid := c.Param("id")
			if !bson.IsObjectIdHex(qid) {
				c.JSON(http.StatusBadRequest, gin.H{"error": "not event id"})
			}
			id := bson.ObjectIdHex(qid)
			// delete object
			if err := db.C("events").RemoveId(id); err == nil {
				c.JSON(http.StatusOK, gin.H{"success": "delete event id=" + qid})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		})
	}

	router.Run(host)
}

func main() {
	session, err := mgo.Dial("mongodb://" + getMongoHost())
	if err != nil {
		log.Panic(err)
	}
	defer session.Close()
	db := session.DB("test")

	runWebServer(db)
}
