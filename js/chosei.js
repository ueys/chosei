window.addEventListener("load", () => {
  let createEventHTML = (event) => {
    let html = ""
    html += '<div class="jumbotron">' +
      '<h1 class="display-4">' + event.title + '</h1>' +
      '<p class="lead">' + event.desc + '</p>' +
      '<hr class="my-4">' +
      '<table class="table"><thead><tr>' +
      '<th scope="col">なまえ</th>'
    event.candidates.forEach(date => {
      html += '<th scope="col">' + date + '</th>'
    })
    html += '</tr></thead>' +
      '<tbody>'
    event.members.forEach(member => {
      html += '<tr>' +
        '<td>' + member.name + '</td>'
      member.status.forEach(check => {
        html += '<td>' + check + '</td>'
      })
      html += '<tr>'
    })
    html += '</tbody>' +
      '</table>' +
      '<p class="lead"><a class="btn btn-primary" href="view/' + event._id + '" role="button">Check</a></p>' +
      '</div>'
    return html
  }

  fetch("../api/v1/events").then(responce => {
    return responce.json()
  }).then(events => {
    let eventView = ""
    events.forEach(event => {
      eventView += createEventHTML(event)
    });
    document.getElementById("events").innerHTML = eventView
  }).catch(errJson => {
    document.getElementById("events").innerHTML = "unnormal status:" + errJson
  })
})