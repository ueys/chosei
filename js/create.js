window.addEventListener("load", () => {
  let title = document.getElementById("event-title")
  let desc = document.getElementById("event-desc")
  let question = document.getElementById("event-q")
  let questionList = document.getElementById("event-q-list")
  let eventJson = {
    "title": "no title",
    "desc": "no description",
    "candidates": [],
    "members": []
  }
  document.getElementById("add-q").addEventListener('click', () => {
    console.log(question.value)
    questionList.innerText += question.value + ", "
    eventJson.candidates.push(question.value)
  })
  document.getElementById("btn-create-event").addEventListener('click', () => {
    eventJson.title = title.value
    eventJson.desc = desc.value
    console.log(eventJson)
    fetch("../api/v1/events", {
      method: "POST",
      body: JSON.stringify(eventJson)
    }).then(responce => {
      return responce.json();
    }).then(respJson => {
      console.log(respJson)
    })
  })
})