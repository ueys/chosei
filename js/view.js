window.addEventListener("load", () => {
  let id = location.pathname.split("/").pop()
  let ev = {}

  let createEventHTML = (event) => {
    let html = ""
    html += '<div class="jumbotron">' +
      '<h1 class="display-4">' + event.title + '</h1>' +
      '<p class="lead">' + event.desc + '</p>' +
      '<hr class="my-4">' +
      '<table class="table"><thead><tr>' +
      '<th scope="col">なまえ</th>'
    event.candidates.forEach(date => {
      html += '<th scope="col">' + date + '</th>'
    })
    html += '</tr></thead>' +
      '<tbody>'
    event.members.forEach(member => {
      html += '<tr>' +
        '<td>' + member.name + '</td>'
      member.status.forEach(check => {
        html += '<td>' + check + '</td>'
      })
      html += '<tr>'
    })
    html += '</tbody>' +
      '</table>' +
      '</div>'
    return html
  }

  let createStatusTable = (event) => {
    event.candidates.forEach(date => {
      html += '<th scope="col">' + date + '</th>'
    })
  }

  document.getElementById("submit").addEventListener('click', () => {
    newMember = {}
    newMember.name = document.getElementById("inputName").value
    ev.members.push(newMember)
    console.log(ev)
    fetch("../../api/v1/events/" + ev._id, {
      method: "PUT",
      body: JSON.stringify(ev)
    }).then(responce => {
      return responce.json();
    }).then(respJson => {
      console.log(respJson)
    }).catch(err => {
      console.error(err)
    })
  })

  fetch("../../api/v1/events/" + id).then(responce => {
    return responce.json()
  }).then(event => {
    ev = event
    document.getElementById("event").innerHTML = createEventHTML(event)
  }).catch(errJson => {
    document.getElementById("event").innerHTML = "unnormal status:" + errJson
  })
})