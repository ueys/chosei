# 調整さんっぽいのを作る

## how to run

### debelop

1. install golang
1. install go package(go get)
1. run or build
1. access <http://localhost:8080/events>

```
$ go run chosei.go
```

### docker

must be build

## API

かく

## 検討とかの話

### 環境選択

- お勉強でGo
- お勉強でGin
- できることならスキャッフォルドしたい
    - gin-scaffoldなるものがあるらしい
    - パッと見だとPOSTのパラメータ渡しがjsonじゃない。jsonでおしゃべりしたい
    - どうせスキーマ変えたくなるしスキャッフォルドするとかえって面倒か
        - 面倒くさいけど書こう
- jsonかえすAPIきりたい
    - rest-fullとか言い出すと面倒くさいのでそこまでは言わない
    - できればDBにもjsonで突っ込みたい
    - 運用が死ぬことで有名なmongoだけど、雑に扱うには楽なんよね……
    - mongoに読み書きするか
        - スケールしない
            - まぁそんな使わん
        - たぶん排他制御で死ぬ
            - まぁそんな使わん

### endpoint

```
[VIEW endpoint]
GET host/events
 -> show all event
GET host/events/<ObjID>
 -> show event

[API endpoint]
GET host/api/v1/events
 -> retuen all event
POST host/api/v1/events
 -> create event
    return json with ObjID
GET host/api/v1/events/<ObjID>
 -> return event json
PUT host/api/v1/events/<ObjID>
 -> update event
DELETE host/api/v1/events/<ObjID>
 -> delete event
``` 

- 普通に考えて排他考慮してないからDB read/write衝突するよね
- DB管理ワーカーを立ててあげて、そいつにqueueする
    - めんどい
- とりあえずdeleteで全部イベントを消せるように作っちゃったけど、delete-flag入れるか否か
    - delete-flagは論争の元だし最初はいれんでいいか
    - 当初の思想として権限管理なんか無くていいやーと思ってたけど、やっぱadminはいるかも

### JSON

#### style 1

```
{
    "title": "event title",
    "desc": "event description",
    "candidate": {
        "1/1": {
            "members": {
                "taro": "no answer",
                "ziro": "ok"
            }
        },
        "1/2": {
            "members": {
                "taro": "ok",
                "ziro": "not ok"
            }
        }
    }
}
```

- これだとメンバーが情報足すたびにcandidates要素を全とっかえになって面倒か
- member要素をforeachして書き換える感じにしたい

#### style 2

```
{
    "title": "event title",
    "desc": "event description",
    "candidates": [
        "1/1",
        "1/2"
    ]
    "members": {
        "taro": {
            "1/1": "no answer",
            "1/2": "ok"
        },
        "ziro": {
            "1/1": "ok",
            "1/2": "not ok"
        }
    }
}
```

- candidatesが冗長だけど、これ持ってないとメンバ無い時に描画できない
- golang的にネストしたmapの扱いは面倒くさそう

#### style 3

```
{
    "title": "event title",
    "desc": "event description",
    "candidates": [
        "1/1",
        "1/2"
    ]
    "members": [
        {
            "name": "taro",
            "status": {
                "1/1": "no answer",
                "1/2": "ok"
            }
        },
        {
            "name": "ziro",
            "status": {
                "1/1": "ok",
                "1/2": "not ok"
            }
        }
    ]
}
```

- statusはuint8で持ってenumすりゃいいか
    - uint8にしたらバグってもうた。mgoわからん
    - まぁ自由度が高いと言えば聞こえはいいしstrでいいか
- candidateとmember.statusの値の整合性は誰が保証するの？
    - candidateはいいけどstatusのほうはmapなんで順序性も担保されないからしんどくね

#### style 4

```
{
    "title": "event title",
    "desc": "event description",
    "candidates": [
        "1/1",
        "1/2"
    ]
    "members": [
        {
            "name": "taro",
            "status": [
                "ok",
                "not ok"
            ]
        },
        {
            "name": "ziro",
            "status": [
                "not ok",
                "ok"
            ]
        }
    ]
}
```

- 入力時点でcandidatesの順序とstatusの順序は維持される前提でたのむ
- 本当はcandidatesは `time.Time` で持ちたいので今後なんとかしたさある

## known Issue

- 削除とかUI側まだ全然つくれてない
- CSS書きたくないけど書かないとネー
- 全部の値を出したり入れたりしていてクソなので、例えばタイトルだけ得る場合は `db.event.find({}, title:true)` みたいな感じに吐けるようにする
    - まだ `mgo` の動きがわかってない
- jsがフレームワーク使ってない生書きでクソofクソ。Web屋がみたらゲロ吐くレベル
    - webpack使いたくなかった
    - jqueryは捨てたかった(bootstrapのjsがjquery依存なんで厳しさあるんだよなー)
    - Vue使いたかったけどこれ以上のお勉強は俺の脳が耐えられない
    - 生jsでできるとこまでやってみるか
        - なにげない判断がコード品質を傷つけた
    - IEは知らん
- エンドポイント設計ミスってる
    - `../../` とかしまくっててアレ
        - ディレクトリトラバーサルでアカンデータを見られたりはしないはず……だけどキモい
